package exercicis_NF2;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

import com.db4o.query.Predicate;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Producte {
    // Variable que fem servir per a passar un valor a un predicate per així poder filtrar la búsqueda en la BD.
    public static Producte_Dades producteDelXML;
    //Creació de productes
    public static void menu1(ObjectContainer db) {
        Producte_Dades[] productes = {
                new Producte_Dades(1, "Ració de combat", 1, 0),
                new Producte_Dades(2, "Torpede mark 2", 100, 2),
                new Producte_Dades(3, "Autopilot SAU-6", 10, 3),
                new Producte_Dades(4, "Sistema inercial MIS-P", 11, 3),
                new Producte_Dades(5, "Bobina d'inducció magnètica del reactor principal", 10000, 4),
                new Producte_Dades(6, "Sistema de navegació de llarg abast RSDN-10", 12, 3)
        };

        //Emmagatzematge a la base de dades
        for (int i = 0; i < productes.length; i++) {
            db.store(productes[i]);
        }
    }

    //Llista per pantalla els productes de la base de dades sense ordre
    public static void menu10(ObjectContainer db) {
        Predicate p = new Predicate<Producte_Dades>() {
            @Override
            public boolean match(Producte_Dades producte) {
                return false;
            }
        };
        List<Producte_Dades> result = db.query(p);
        for (Producte_Dades producteTmp : result) {
            System.out.println(producteTmp.toString());
        }
    }

    //Exportar els productes a un XML
    public static void menu20(ObjectContainer db, String pathAtxiusXML) {
        ArrayList<Producte_Dades> llistaOrdenadaProductes;
        XMLEncoder e = null;

        Predicate p = new Predicate<Producte_Dades>() {
            @Override
            public boolean match(Producte_Dades producte) {
                return true;
            }
        };
        List<Producte_Dades> result = db.query(p);
        llistaOrdenadaProductes = new ArrayList<Producte_Dades>();
        llistaOrdenadaProductes.addAll(result);
        //ordenem els productes alfabèticament
        Collections.sort(llistaOrdenadaProductes);

        try {
            e = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(pathAtxiusXML)));
            for (Producte_Dades producteTmp : llistaOrdenadaProductes) {
                e.writeObject(producteTmp);
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } finally {
            e.close();
        }

    }

    //Importar els productes que hi ha al XML a la nostra BD
    public static void menu21(ObjectContainer db, String pathArxiusXML) {
        XMLDecoder d = null;

        try {
            d = new XMLDecoder(new BufferedInputStream(new FileInputStream(pathArxiusXML)));
            while ((producteDelXML = (Producte_Dades) d.readObject()) != null) {
                Predicate p1 = new Predicate<Producte_Dades>() {
                    @Override
                    public boolean match(Producte_Dades producte) {
                        return producte.getProducteId() == producteDelXML.getProducteId();
                    }
                };
                ObjectSet<Producte_Dades> result = db.query(p1);
                if (!result.isEmpty()) {
                    //trobat un producte amb el mateix id que l'XML a la BD
                    Producte_Dades producteTrobatALaBD = result.next(); //ara producteTrobatALaBD apunta al mateix objecte que el de la BD que coincideix
                    System.out.println("menu21(): UPDATE");
                    //Dades abans del canvi
                    System.out.println("producte ID: " + producteTrobatALaBD.getProducteId() + ", nom: " + producteTrobatALaBD.getProducteNom() + ", preu: " + producteTrobatALaBD.getProductePreu());
                    System.out.println("canviat a");
                    //Dades noves
                    System.out.println("producte ID: " + producteDelXML.getProducteId() + ", nom: " + producteDelXML.getProducteNom() + ", preu: " + producteDelXML.getProductePreu());
                    System.out.println();
                    //canviem el producte de la base de dades amb les dades del XML
                    producteTrobatALaBD.setProducteNom(producteDelXML.getProducteNom());
                    producteTrobatALaBD.setProductePreu(producteDelXML.getProductePreu()*2);
                    db.store(producteTrobatALaBD);
                } else {
                    System.out.println("menu21(): INSERT");
                    System.out.println("producte ID: " + producteDelXML.getProducteId() + ", nom: " + producteDelXML.getProducteNom() + ", preu: " + producteDelXML.getProductePreu());
                    System.out.println();
                    db.store(producteDelXML);
                }
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            // Així controlem que hem arribat al final del fitxer perquè el "d.readObject() != null" no va, ja que, ha intentat
            // llegir un objecte quan no n'hi havia cap i per tant ja no arriba ni a comparar-lo amb NULL.
            System.out.println("NO HI HA MÉS PRODUCTES AL FITXER " + pathArxiusXML);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            System.out.println();
            System.out.println("TANQUEM EL FITXER " + pathArxiusXML);
            d.close();
        }
    }
}
