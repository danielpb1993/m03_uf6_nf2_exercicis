package exercicis_NF2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

public class Encarrec_Dades {
    private int idEncarrec;
    private int idDepartamentQueElDemana;
    private ArrayList<Producte_Dades> llistaProductes = new ArrayList<Producte_Dades>();
    private LocalDateTime data;

    public Encarrec_Dades() {
    }

    public Encarrec_Dades(int idEncarrec, int idDepartamentQueElDemana, LocalDateTime data) {
        this.idEncarrec = idEncarrec;
        this.idDepartamentQueElDemana = idDepartamentQueElDemana;
        this.data = data;
    }

    public int getIdEncarrec() {
        return idEncarrec;
    }

    public void setIdEncarrec(int idEncarrec) {
        this.idEncarrec = idEncarrec;
    }

    public int getIdDepartamentQueElDemana() {
        return idDepartamentQueElDemana;
    }

    public void setIdDepartamentQueElDemana(int idDepartamentQueElDemana) {
        this.idDepartamentQueElDemana = idDepartamentQueElDemana;
    }

    public ArrayList<Producte_Dades> getLlistaProductes() {
        return llistaProductes;
    }

    public void setLlistaProductes(ArrayList<Producte_Dades> llistaProductes) {
        this.llistaProductes = llistaProductes;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public void addProducte(Producte_Dades producteTmp) {
        this.llistaProductes.add(producteTmp);
    }

    @Override
    public String toString() {
        String res = "id " +  idEncarrec + ", del departament " + IKSRotarranConstants.DEPARTAMENTS[idDepartamentQueElDemana] + ", productes: ";
        Iterator<Producte_Dades> it = getLlistaProductes().iterator();
        while (it.hasNext()) {
            Producte_Dades producteTmp = it.next();
            if (producteTmp != null) {
                res += producteTmp.getProducteNom() + ", ";
            } else {
                res += "No existeix (null), ";
            }
        }
        res.substring(0, res.length() - 2);
        return res;
    }
}
