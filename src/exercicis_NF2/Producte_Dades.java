package exercicis_NF2;

import java.text.Collator;
import java.util.Locale;

public class Producte_Dades implements Comparable<Producte_Dades> {
    private int producteId;
    private String producteNom;
    private int productePreu;
    private int idDepartamentQueElPotDemanar;

    public Producte_Dades() {
    }

    public Producte_Dades(int producteId, String producteNom, int productePreu, int idDepartamentQueElPotDemanar) {
        this.producteId = producteId;
        this.producteNom = producteNom;
        this.productePreu = productePreu; //TODO darksets
        this.idDepartamentQueElPotDemanar = idDepartamentQueElPotDemanar;
    }

    public int getProducteId() {
        return producteId;
    }

    public void setProducteId(int producteId) {
        this.producteId = producteId;
    }

    public String getProducteNom() {
        return producteNom;
    }

    public void setProducteNom(String producteNom) {
        this.producteNom = producteNom;
    }

    public int getProductePreu() {
        return productePreu;
    }

    public void setProductePreu(int productePreu) {
        this.productePreu = productePreu;
    }

    public int getIdDepartamentQueElPotDemanar() {
        return idDepartamentQueElPotDemanar;
    }

    public void setIdDepartamentQueElPotDemanar(int idDepartamentQueElPotDemanar) {
        this.idDepartamentQueElPotDemanar = idDepartamentQueElPotDemanar;
    }

    @Override
    public String toString() {
        String res = producteId + ". " + producteNom + " (" + productePreu + " darseks)";
        return res;
    }

    @Override
    public int compareTo(Producte_Dades obj) {
        Collator comparador = Collator.getInstance(new Locale("es"));
        comparador.setStrength(Collator.PRIMARY);
        return comparador.compare(this.getProducteNom(), obj.getProducteNom());
    }
}
