package exercicis_NF2;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import java.time.LocalDateTime;
import java.util.List;

public class Encarrec {
    //Creació d'encàrrecs
    public static void menu2(ObjectContainer db) {
        Encarrec_Dades[] encarrecs = {
                new Encarrec_Dades(0, 1, LocalDateTime.now()),
                new Encarrec_Dades(1, 1, LocalDateTime.now()),
                new Encarrec_Dades(2, 3, LocalDateTime.now()),
                new Encarrec_Dades(3, 1, LocalDateTime.now()),
                new Encarrec_Dades(4, 1, LocalDateTime.now()),
                new Encarrec_Dades(5, 4, LocalDateTime.now()),
                new Encarrec_Dades(6, 3, LocalDateTime.now())
        };

        for (int i = 0; i < encarrecs.length; i++) {
            db.store(encarrecs[i]);
        }
    }

    //Llistar tots els encàrrecs de la base de dades
    public static void menu11(ObjectContainer db) {
        Predicate p = new Predicate<Encarrec_Dades>() {
            @Override
            public boolean match(Encarrec_Dades e) {
                return true;
            }
        };
        List<Encarrec_Dades> result = db.query(p);
        for (Encarrec_Dades encarrecTmp : result) {
            System.out.println(encarrecTmp.toString());
        }
    }
}
