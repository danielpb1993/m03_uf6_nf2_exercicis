package exercicis_NF2;

import Varies.Cadena;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;

import java.util.*;

public class Departament {
    //Objecte tipus Producte_Dades declarat per menu14();
    private static Producte_Dades producteSeleccionat = null;
    //Creació departaments
    public static void menu3(ObjectContainer db) {
        Departament_Dades departament;
        String email;
        String telefon;

        for (int i = 1; i < IKSRotarranConstants.DEPARTAMENTS.length; i++) {
            email = IKSRotarranConstants.DEPARTAMENTS[i] + "@IKSRotarran.ik";
            telefon = "";
            for (int j = 1; j < 9; j++) {
                telefon = telefon + String.valueOf(i);
            }
            //Emmagatzamatge de departaments a la base de dades
            departament = new Departament_Dades(i, IKSRotarranConstants.DEPARTAMENTS[i], email, telefon);
            db.store(departament);
        }
    }
    //Assginar els encàrrecs als departaments corresponents i els productes
    //als encàrrecs

    //Busquem els departaments de la bbdd
    public static void menu4(ObjectContainer db) {
        Predicate p = new Predicate<Departament_Dades>() {
            @Override
            public boolean match(Departament_Dades d) {
                return true;
            }
        };
        //Els guardem a un ObjectSet
        ObjectSet<Departament_Dades> result = db.query(p);
        //Recorrem aquest ObjectSet
        while (result.hasNext()) {
            Departament_Dades departamentTmp = result.next();
            Predicate p2 = new Predicate<Encarrec_Dades>() {
                // Busquem els encàrrecs en els que coincideix l'id del departament
                // i del departament que pot demanar l'encàrrec
                @Override
                public boolean match(Encarrec_Dades e) {
                    return e.getIdDepartamentQueElDemana() == departamentTmp.getId();
                }
            };
            //Guardem els encàrrecs a un OBjectSet
            ObjectSet<Encarrec_Dades> result2 = db.query(p2);
            //El recorrem i afegim cada encàrrec al departament corresponent
            while (result2.hasNext()) {
                Encarrec_Dades encarrecTmp = result2.next();
                departamentTmp.afegirEncarrec(encarrecTmp);
            }

            db.store(departamentTmp);
        }

        //Busquem el productes
        Predicate p3 = new Predicate<Producte_Dades>() {
            @Override
            public boolean match(Producte_Dades p) {
                return true;
            }
        };
        //els guardem en un List
        List<Producte_Dades> result3 = db.query(p3);
        //Recorrem aquest List per a trobar els departaments que poden demanar cada producte
        for (Producte_Dades producteTmp : result3) {
            Predicate p4 = new Predicate<Departament_Dades>() {
                @Override
                public boolean match(Departament_Dades d) {
                    if (d.getId() == producteTmp.getIdDepartamentQueElPotDemanar() || producteTmp.getIdDepartamentQueElPotDemanar() == 0){
                        return true;
                    } else {
                        return false;
                    }
                }
            };
            //Guardem aquests departaments a un ObjectSet
            ObjectSet<Departament_Dades> result4 = db.query(p4);
            //Recorrem aquest ObjectSet
            while (result4.hasNext()) {
                Departament_Dades departamentTmp = result4.next();
                //Recorrem la llista d'encàrrecs d'aquests departaments
                Iterator<Encarrec_Dades> it = departamentTmp.getLlistaEncarrecs().iterator();
                //Afegim a cada encàrrec els productes
                while (it.hasNext()) {
                    Encarrec_Dades encarrecTmp = it.next();
                    encarrecTmp.addProducte(producteTmp);
                }
            }
        }
    }

    //llistar tots els departaments
    public static void menu12(ObjectContainer db) {
        Predicate p = new Predicate<Departament_Dades>() {
            @Override
            public boolean match(Departament_Dades d) {
                return true;
            }
        };
        List<Departament_Dades> result = db.query(p);
        for (Departament_Dades departamentTmp : result) {
            System.out.println(departamentTmp.getId() + ". " + departamentTmp.getNom());
        }
    }

    //llistar tots els departaments. per a cada un els seus encàrrecs. i per a cada encàrrec els sues productes
    public static void menu13(ObjectContainer db) {
        Predicate p = new Predicate<Departament_Dades>() {
            @Override
            public boolean match(Departament_Dades d) {
                return true;
            }
        };
        ObjectSet<Departament_Dades> result = db.query(p);
        while (result.hasNext()) {
            Departament_Dades departamentTmp = result.next();
            System.out.println(departamentTmp);
            System.out.println();
        }
    }

    //llistar els departaments que hagin encarregat un producte concret
    public static void menu14(ObjectContainer db) {
        String opcio;
        boolean existeixenErrors;
        int numProducteSeleccionat;
        //declarem un arraylist on guardarem la llista de productes ordenada
        ArrayList<Producte_Dades> llistaOrdenadaProductes;

        //LListem els productes de la bbdd
        Predicate p = new Predicate<Producte_Dades>() {
            @Override
            public boolean match(Producte_Dades producte) {
                return true;
            }
        };
        List<Producte_Dades> result = db.query(p);
        llistaOrdenadaProductes = new ArrayList<Producte_Dades>();
        llistaOrdenadaProductes.addAll(result);
        //Ordenem la llista
        Collections.sort(llistaOrdenadaProductes);
        //Declarem un int per a mostrar el número de producte que ha de seleccionar l'usuari
        int i = 0;
        System.out.println("Llista de productes:");
        for (Producte_Dades producteTmp : llistaOrdenadaProductes) {
            System.out.println(i + ": " + producteTmp.getProducteNom() + " (" + producteTmp.getProductePreu() + " darseks)");
            i++;
        }
        System.out.println();

        existeixenErrors = true;
        do {
            existeixenErrors = false;
            Scanner sc = new Scanner(System.in);
            System.out.println("Selecciona un producte");
            opcio = sc.next();

            if (Cadena.stringIsInt(opcio)) {
                numProducteSeleccionat = Integer.parseInt(opcio);
                if (numProducteSeleccionat >= 0 && numProducteSeleccionat < result.size()) {
                    producteSeleccionat = llistaOrdenadaProductes.get(numProducteSeleccionat);
                } else {
                    System.out.println("Error: has seleccionat un producte que no existeix");
                    System.out.println();
                    existeixenErrors = true;
                }
            } else {
                System.out.println("Error: has introduït " + opcio + " com a producte. Has d'introduïr un integer");
                existeixenErrors = true;
            }
        } while (existeixenErrors);

        //Busquem els encàrrecs que tenen el producte seleccionat
        Predicate p2 = new Predicate<Encarrec_Dades>() {
            @Override
            public boolean match(Encarrec_Dades e) {
                return e.getLlistaProductes().contains(producteSeleccionat);
            }
        };
        ObjectSet<Encarrec_Dades> result2 = db.query(p2);
        System.out.println();
        System.out.println("Trobats " + result2.size() + " encàrrecs amb el producte \"" + producteSeleccionat.getProducteNom() + "\".");
        while (result2.hasNext()) {
            Encarrec_Dades encarrecTmp = result2.next();
            Predicate p3 = new Predicate<Departament_Dades>() {
                @Override
                public boolean match(Departament_Dades d) {
                    return d.getLlistaEncarrecs().contains(encarrecTmp);
                }
            };
            ObjectSet<Departament_Dades> result3 = db.query(p3);
            if (result3.size() == 0) {
                System.out.println("Error: No s'ha trobat cap departament que tingui assignat l'encarrec amb ID " + encarrecTmp.getIdEncarrec());
            } else {
                while (result3.hasNext()) {
                    Departament_Dades departamentTmp = result3.next();
                    System.out.println(departamentTmp.toStringSencill(encarrecTmp));
                }
            }
        }
    }

    public static void menu40(ObjectContainer db) {
        int departamentAESborrar = 4;
        Predicate p1 = new Predicate<Departament_Dades>() {
            @Override
            public boolean match(Departament_Dades d) {
                return d.getId() == departamentAESborrar;
            }
        };
        ObjectSet<Departament_Dades> result = db.query(p1);
        if (result.size() != 1) {
            System.out.println("No es pot eliminar aquest departament. N'hi ha més d'1 o 0 amb el mateix ID en la BD.");
        } else {
            Departament_Dades d = result.next();
            System.out.println("ELIMINAT EL DEPARTAMENT " + d.getNom());
            db.delete(d);
        }
    }

    public static ObjectContainer menu41(ObjectContainer db) {
        int departamentAEsborrar;


        // Tanquem la BD perquè l'hem d'obrir afegint l'opció d'esborrar en CASCADE.
        db.close();

        // Aquesta configuració és perquè esborri en cascada quan fem un delete de qualsevol objecte del nostre
        // programa (<Departament_Dades>, <Encarrec_Dades> o <Producte_Dades>) que contingui altres objectes.
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Departament_Dades.class).cascadeOnDelete(true);
        config.common().objectClass(Encarrec_Dades.class).cascadeOnDelete(true);
        config.common().objectClass(Producte_Dades.class).cascadeOnDelete(true);
        db = Db4oEmbedded.openFile(config, IKSRotarranConstants.pathBD);

        departamentAEsborrar = 1;

        Predicate p = new Predicate<Departament_Dades>() {
            @Override
            public boolean match(Departament_Dades d) {
                if (d.getId() == departamentAEsborrar) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        ObjectSet<Departament_Dades> result = db.query(p);

        if (result.size() != 1) {
            System.out.println("No es pot eliminar aquest departament. N'hi ha més d'1 o 0 amb el mateix ID en la BD.");
        } else {
            Departament_Dades d = result.next();

            System.out.println("ELIMINAT EN CASCADA EL DEPARTAMENT " + d.getNom());

            db.delete(d);
        }

        // Retornem la BD sense la capacitat d'esborrar en cascada.
        db.close();
        config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Departament_Dades.class).cascadeOnUpdate(true);
        config.common().objectClass(Encarrec_Dades.class).cascadeOnUpdate(true);
        config.common().objectClass(Producte_Dades.class).cascadeOnUpdate(true);
        db = Db4oEmbedded.openFile(config, IKSRotarranConstants.pathBD);

        return db;
    }

}
